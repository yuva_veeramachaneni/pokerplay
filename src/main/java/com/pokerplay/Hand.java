package com.pokerplay;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

public class Hand implements Comparable<Hand> {

	private List<Card> cards;

	private Map<Integer, List<Card>> rankValueMap;

	private Map<Integer, List<Card>> suitValueMap;

	private Set<Integer> rankValueSet;
	private Set<Integer> suitSet;

	private int tieBreakRank;

	public Hand(List<Card> cards) {
		this.cards = cards;

		rankValueMap = this.cards.stream()
				.collect(Collectors.groupingBy(card -> card.getRank().getValue(), mapping(card -> card, toList())));

		suitValueMap = this.cards.stream()
				.collect(Collectors.groupingBy(card -> card.getSuit().getValue(), mapping(card -> card, toList())));

		this.rankValueSet = rankValueMap.keySet();
		this.suitSet = suitValueMap.keySet();
	}
	

	public static Hand fromString(String cardStr) {
		List<Card> cards = new ArrayList<Card>();
		String[] cardTokens = cardStr.split("\\s+");
		for (String cardToken : cardTokens) {
			cards.add(Card.fromString(cardToken));
		}
		return new Hand(cards);
	}

	public double getScore() {

		if (isRoyalFlush()) {
			// Hard coded the tie breaker value for now, as two royal flushes
			// will be a tie-as we are not comparing the suit values for
			// now.Below need to be check for suit just in take if we need to
			// use suit values to break the tie
			setTieBreakerValues(100);
			return 10;
		} else if (isStraightFlush()) {
			setTieBreakerValues(getMaxValue());
			return 9;
		} else if (isFourOfAKind().isPresent()) {
			setTieBreakerValues(isFourOfAKind().getAsInt());
			return 8;
		} else if (isFullHouse().isPresent()) {
			setTieBreakerValues(isFullHouse().getAsInt());
			return 7;
		} else if (isFlush()) {
			setTieBreakerValues(getMaxValue());
			return 6;
		} else if (isStraight()) {
			setTieBreakerValues(getMaxValue());
			return 5;
		} else if (isThreeOfAKind().isPresent()) {
			setTieBreakerValues(isThreeOfAKind().getAsInt());
			return 4;
		} else if (isTwoPair().isPresent()) {
			setTieBreakerValues(isTwoPair().getAsInt());
			return 3;
		} else if (isPair().isPresent()) {
			setTieBreakerValues(isPair().getAsInt());
			return 2;
		} else {
			return 1;
		}

	}
	
	/**
	 * Check if the hand has one pairs and returns the values of the pair as
	 * int to be set as a tieBreakRank just in case if the other player has the
	 * same score/pairs
	 * 
	 * @return OptionalInt
	 */
	public OptionalInt isPair() {

		List<Card> pairList = getPairList();

		if (!CollectionUtils.isEmpty(pairList) && pairList.size() == 1) {
			return OptionalInt.of(pairList.get(0).getRank().getValue());
		}

		return OptionalInt.empty();
	}

	private List<Card> getPairList() {
		return this.rankValueMap.entrySet().stream().filter(entry -> entry.getValue().size() == 2)
				.map(entry -> entry.getValue().get(0)).collect(toList());
	}

	/**
	 * Check if the hand has two pairs and returns the values of the pairs as
	 * int to be set as a tieBreakRank just in case if the other player has the
	 * same score/pairs
	 * 
	 * @return OptionalInt
	 */
	public OptionalInt isTwoPair() {

		List<Card> pairList = getPairList();

		if (!CollectionUtils.isEmpty(pairList) && pairList.size() == 2) {
			return OptionalInt.of(pairList.stream().map(Card::getRank).mapToInt(m -> m.getValue()).sum());
		}

		return OptionalInt.empty();
	}

	/**
	 * Check if the hand has threeOfKind and returns the value of the three set
	 * as int to be set as a tieBreakRank just in case if the other player has
	 * the same score/pairs
	 * 
	 * @return OptionalInt
	 */

	public OptionalInt isThreeOfAKind() {

		Optional<Card> threeOfAKind = this.rankValueMap.entrySet().stream()
				.filter(entry -> entry.getValue().size() == 3).map(entry -> entry.getValue().get(0)).findAny();

		if (threeOfAKind.isPresent()) {
			return OptionalInt.of(threeOfAKind.get().getRank().getValue());
		}

		return OptionalInt.empty();
	}

	/**
	 * @return boolean
	 */
	public boolean isStraight() {

		int maxValue = getMaxValue();
		int minValue = getMinValue();

		if (maxValue - minValue == 4 && rankValueSet.size() == 5) {
			return true;
		}

		return false;
	}

	private int getMinValue() {
		return rankValueSet.stream().mapToInt(m -> m).min().getAsInt();
	}

	private int getMaxValue() {
		return rankValueSet.stream().mapToInt(m -> m).max().getAsInt();
	}

	public boolean isFlush() {

		if (suitSet.size() == 1) {
			return true;
		}

		return false;
	}

	public OptionalInt isFullHouse() {
		// Full house is three of kind + a pair which is nothing but
		// rankValueSet size to be 2. We are returning three of kind one as a
		// tieBrenkRank, if the tieBreakRank is also same then it will default
		// to finding the next highest valued in hand
		if (isThreeOfAKind().isPresent() && rankValueSet.size() == 2) {
			return isThreeOfAKind();
		}

		return OptionalInt.empty();
	}

	public OptionalInt isFourOfAKind() {

		Optional<Card> fourOfAKind = this.rankValueMap.entrySet().stream().filter(entry -> entry.getValue().size() == 4)
				.map(entry -> entry.getValue().get(0)).findAny();

		if (fourOfAKind.isPresent()) {
			return OptionalInt.of(fourOfAKind.get().getRank().getValue());
		}

		return OptionalInt.empty();
	}

	public boolean isStraightFlush() {
		// a straight flush is both a straight and flush
		if (isStraight() && isFlush()) {
			return true;
		}

		return false;
	}

	public boolean isRoyalFlush() {
		// royal flush is a straight flush with max value being ACE which is 14
		if (isStraightFlush() && getMaxValue() == 14) {
			return true;

		}

		return false;
	}

	/*
	 * Compares the score of the current hand with other hand If the main score
	 * tie, then we will use tieBreakRank. If the tieBreakRank is also tie then
	 * we will use the highest value to break the tie in a recursive way.
	 * 
	 * Return 1 if the current hand is winner compared to other hand and -1
	 * viceversa. 0 is retuned in case of tie
	 */
	public int compareTo(Hand other) {
		if (this.getScore() != other.getScore()) {
			return new Double(this.getScore()).compareTo(new Double(other.getScore()));
		}

		if (this.tieBreakRank != other.getTieBreakRank()) {
			return new Double(this.getTieBreakRank()).compareTo(new Double(other.getTieBreakRank()));
		}

		if (this.getMaxValue() != other.getMaxValue()) {
			return new Integer(this.getMaxValue()).compareTo(new Integer(other.getMaxValue()));
		}

		return compareNextHighestValues(other, getMaxValue());
	}

	/**
	 * Breaks the tie by recursively checking the highest value to break the
	 * tie, in case where a tiebreak rank is also a tie
	 * 
	 * Example 3C 3D 3H 5S TS :  3C 3D 3H 4D TS 
	 * score          4: 4 
	 * tieBreakRank 3 :3
	 * HighestValue 5:4 Player1 wins !!!!
	 * 
	 * @param other
	 * @param maxValue
	 * @return
	 */
	private int compareNextHighestValues(Hand other, int maxValue) {
		int currentPlayerNextHighestValue = this.getSecondHighestValue(maxValue);
		int otherPlayerNextHighestValue = other.getSecondHighestValue(maxValue);

		if (currentPlayerNextHighestValue != otherPlayerNextHighestValue) {
			return new Integer(currentPlayerNextHighestValue).compareTo(new Integer(otherPlayerNextHighestValue));
		}

		if (currentPlayerNextHighestValue != 0) {
			return this.compareNextHighestValues(other, currentPlayerNextHighestValue);
		}

		return 0;
	}
	
	/**
	 * Set the tie break value, to break the tie if the main score is a tie. In
	 * case of pairs and NofAKind this value will be max value of the forming
	 * group. Example 3D 4S 3S 7H TC and 4D 3S 4S 8H JC since both are pairs,
	 * tieBreakRank is set as 3 and 4 for the sets respectively
	 * 
	 * @param tieBreakRank
	 */
	private void setTieBreakerValues(int tieBreakRank) {
		this.tieBreakRank = tieBreakRank;

	}

	/**
	 * @param cMaxValue
	 * @return get the highest value excluding the cMaxValue given
	 */
	public int getSecondHighestValue(int cMaxValue) {
		OptionalInt nextMaxValue = rankValueSet.stream().filter(v -> v < cMaxValue).mapToInt(m -> m).max();
		if (nextMaxValue.isPresent()) {
			return nextMaxValue.getAsInt();
		}
		return 0;
	}

	public int getTieBreakRank() {
		return tieBreakRank;
	}

	public void setTieBreakRank(int tieBreakRank) {
		this.tieBreakRank = tieBreakRank;
	}
	
	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

}
