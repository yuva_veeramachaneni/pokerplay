package com.pokerplay;

import java.util.Arrays;




public class Dealer  {
	
	private Hand player1;
	private Hand player2;


	public Dealer(String playersCards){
		 this.player1 = Hand.fromString(playersCards.substring(0, 15));
         this.player2 = Hand.fromString(playersCards.substring(15, playersCards.length()));
		
	}
	
	/**
	 * Compares both the players and returns the result
	 * @return
	 */
	public int getPlayResult(){

		return  this.player1.compareTo(this.player2);
	}
	
}
