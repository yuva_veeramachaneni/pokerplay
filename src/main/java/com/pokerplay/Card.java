package com.pokerplay;

public class Card {

	private Rank rank;
	private Suit suit;

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

	public Suit getSuit() {
		return suit;
	}

	public void setSuit(Suit suit) {
		this.suit = suit;
	}

	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	public static Card fromString(String rankAndSuit) {
		char[] result = rankAndSuit.toCharArray();
		Rank currentRank = Rank.fromShortName(result[0]);
		Suit currentSuit = Suit.fromShortName(result[1]);
		return new Card(currentRank, currentSuit);
	}

}
