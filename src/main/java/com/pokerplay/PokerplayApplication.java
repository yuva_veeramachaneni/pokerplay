package com.pokerplay;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Arrays;
import static java.lang.System.out;

@SpringBootApplication
public class PokerplayApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PokerplayApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		//Open the file with first passed argument.
        FileInputStream fstream = new FileInputStream(args[0]);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;
        
        int player1WinCount =0;
    	int player2WinCount =0;
        //Read File Line By Line
        while ((strLine = br.readLine()) != null)   {
        	
        	//reads each line and prepares the players 
        	Dealer deal = new Dealer(strLine);
        	
        	//get the result for each line which is a single play between two players
        	int result = deal.getPlayResult();
        	
        	int tieCount =0;
        	if(result==1){
        		player1WinCount++;
        	}else if(result==-1){
        		player2WinCount++;
        	}else{
        		tieCount++;
        	}
         
        }
        
        // Print the content on the console
        System.out.println ("********************************* Player 1 "+ player1WinCount + " hands **********************************");
        System.out.println ("********************************* Player 2 "+ player2WinCount + " hands **********************************");
        //Close the input stream
        br.close();
		
	}
}
