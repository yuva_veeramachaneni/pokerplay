package com.pokerplay;

import java.util.Arrays;

public enum Rank {
	
	 TWO('2',2 ),
     THREE( '3',3),
     FOUR('4',4),
     FIVE('5',5),
     SIX('6',6),
     SEVEN('7',7),
     EIGHT( '8',8),
     NINE( '9',9),
     TEN( 'T',10),
     JACK('J',11),
     QUEEN('Q',12),
     KING('K',13),
     ACE ('A',14);
	
	private final char shortName;
	private final int value;
	
    private Rank(char shortName, int value) {
        this.shortName = shortName;
        this.value = value;
    }
    
    public char getShortName() {
		return shortName;
	}

	public int getValue() {
		return value;
	}

	public static Rank fromShortName(char shortName) {
		for (Rank rank : Rank.values()) {
            if (rank.getShortName() == shortName) {
                return rank;
            }
        }
		
		return null;
	}

}
