package com.pokerplay;

public enum Suit {
		
	 HEART('H', 0),
     DIAMOND('D', 1),
     CLUB('C', 2),
     SPADE('S', 3);
	
	private final char shortName;
	private final int value;
	
    private Suit(char shortName, int value) {
        this.shortName = shortName;
        this.value = value;
    }
    
    public char getShortName() {
		return shortName;
	}

	public int getValue() {
		return value;
	}
	
	public static Suit fromShortName(char shortName) {
		for (Suit suit : Suit.values()) {
            if (suit.getShortName() == shortName) {
                return suit;
            }
        }
		
		return null;
	}

}
