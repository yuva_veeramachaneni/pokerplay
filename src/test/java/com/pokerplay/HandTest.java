package com.pokerplay;

import java.util.*;


public class HandTest extends junit.framework.TestCase {
	private ArrayList<Card> pairCards;
	private ArrayList<Card> pairCards2;
	private ArrayList<Card> twoPairCards;
	private ArrayList<Card> threeOfAKindCards;
	private ArrayList<Card> flushCards;
	private ArrayList<Card> straightCards;
	private ArrayList<Card> fullHouseCards;
	private ArrayList<Card> fourOfAKindCards;
	private ArrayList<Card> straightFlushCards;
	
	private ArrayList<Card> scoreOneHighestNumberCards;
	private ArrayList<Card> scoreOneLeastNumberCards;
	
	private ArrayList<Card> royalFlushCards;
	private ArrayList<Card> royalFlushCards2;
	
	private Hand pair;
	private Hand pair2;
	private Hand twoPair;
	private Hand threeOfAKind;
	private Hand straight;
	private Hand flush;
	private Hand fullHouse;
	private Hand fourOfAKind;
	private Hand straightFlush;	
	
	private Hand scoreOneHighestNumber;
	private Hand scoreOneLeastNumber;
	
	private Hand royalFlushHand;
	private Hand royalFlushHand2;

	public HandTest() {
		pairCards = new ArrayList<Card>();
		pairCards2 = new ArrayList<Card>();
		twoPairCards = new ArrayList<Card>();
		threeOfAKindCards = new ArrayList<Card>();
		straightCards = new ArrayList<Card>();
		flushCards = new ArrayList<Card>();
		fullHouseCards = new ArrayList<Card>();
		fourOfAKindCards = new ArrayList<Card>();
		straightFlushCards = new ArrayList<Card>();
		scoreOneHighestNumberCards = new ArrayList<Card>();
		scoreOneLeastNumberCards = new ArrayList<Card>();
		
		royalFlushCards = new ArrayList<Card>();
		royalFlushCards2 = new ArrayList<Card>();
		
		
		
		royalFlushCards.add(Card.fromString("JS"));
		royalFlushCards.add(Card.fromString("KS"));
		royalFlushCards.add(Card.fromString("QS"));
		royalFlushCards.add(Card.fromString("AS"));
		royalFlushCards.add(Card.fromString("TS"));
		
		royalFlushCards2.add(Card.fromString("JD"));
		royalFlushCards2.add(Card.fromString("KD"));
		royalFlushCards2.add(Card.fromString("QD"));
		royalFlushCards2.add(Card.fromString("AD"));
		royalFlushCards2.add(Card.fromString("TD"));
		
		scoreOneHighestNumberCards.add(Card.fromString("2S"));
		scoreOneHighestNumberCards.add(Card.fromString("5H"));
		scoreOneHighestNumberCards.add(Card.fromString("9S"));
		scoreOneHighestNumberCards.add(Card.fromString("6S"));
		scoreOneHighestNumberCards.add(Card.fromString("TD"));
		
		scoreOneLeastNumberCards.add(Card.fromString("4S"));
		scoreOneLeastNumberCards.add(Card.fromString("7H"));
		scoreOneLeastNumberCards.add(Card.fromString("3S"));
		scoreOneLeastNumberCards.add(Card.fromString("5S"));
		scoreOneLeastNumberCards.add(Card.fromString("9D"));
		
		
		pairCards.add(Card.fromString("5S"));
		pairCards.add(Card.fromString("5H"));
		pairCards.add(Card.fromString("3S"));
		pairCards.add(Card.fromString("6S"));
		pairCards.add(Card.fromString("TD"));
		
		pairCards2.add(Card.fromString("3S"));
		pairCards2.add(Card.fromString("5H"));
		pairCards2.add(Card.fromString("3S"));
		pairCards2.add(Card.fromString("6S"));
		pairCards2.add(Card.fromString("AD"));
		
		
		twoPairCards.add(Card.fromString("3S"));
		twoPairCards.add(Card.fromString("5H"));
		twoPairCards.add(Card.fromString("3S"));
		twoPairCards.add(Card.fromString("5S"));
		twoPairCards.add(Card.fromString("TD"));
		
		threeOfAKindCards.add(Card.fromString("3S"));
		threeOfAKindCards.add(Card.fromString("5H"));
		threeOfAKindCards.add(Card.fromString("3S"));
		threeOfAKindCards.add(Card.fromString("3D"));
		threeOfAKindCards.add(Card.fromString("TD"));
		
		straightCards.add(Card.fromString("3C"));
		straightCards.add(Card.fromString("4H"));
		straightCards.add(Card.fromString("5S"));
		straightCards.add(Card.fromString("6D"));
		straightCards.add(Card.fromString("7S"));
		
		
		flushCards.add(Card.fromString("3C"));
		flushCards.add(Card.fromString("TC"));
		flushCards.add(Card.fromString("5C"));
		flushCards.add(Card.fromString("6C"));
		flushCards.add(Card.fromString("7C"));
		

		fullHouseCards.add(Card.fromString("3C"));
		fullHouseCards.add(Card.fromString("5C"));
		fullHouseCards.add(Card.fromString("5S"));
		fullHouseCards.add(Card.fromString("3D"));
		fullHouseCards.add(Card.fromString("3H"));
	
		fourOfAKindCards.add(Card.fromString("3C"));
		fourOfAKindCards.add(Card.fromString("3S"));
		fourOfAKindCards.add(Card.fromString("3H"));
		fourOfAKindCards.add(Card.fromString("3D"));
		fourOfAKindCards.add(Card.fromString("TS"));
		
		straightFlushCards.add(Card.fromString("6C"));
		straightFlushCards.add(Card.fromString("7C"));
		straightFlushCards.add(Card.fromString("8C"));
		straightFlushCards.add(Card.fromString("9C"));
		straightFlushCards.add(Card.fromString("TC"));
		

		
		royalFlushHand = new Hand(royalFlushCards);
		royalFlushHand2 = new Hand(royalFlushCards2);
		
		scoreOneHighestNumber = new Hand(scoreOneHighestNumberCards);
		scoreOneLeastNumber = new Hand(scoreOneLeastNumberCards);
		pair2 = new Hand(pairCards2);
		pair = new Hand(pairCards);
		twoPair = new Hand(twoPairCards);
		threeOfAKind = new Hand(threeOfAKindCards);
		straight = new Hand(straightCards);
		flush = new Hand(flushCards);
		fullHouse = new Hand(fullHouseCards);
		fourOfAKind = new Hand(fourOfAKindCards);
		straightFlush = new Hand(straightFlushCards);
	}
	
	/**
	 * Test pair ranking.
	 */
	public void testPair() {
		assertTrue(pair.isPair().isPresent());
	}
	
	/**
	 * Test two pair ranking.
	 */
	public void testTwoPair() {
		assertTrue(twoPair.isTwoPair().isPresent());
	}
	
	/**
	 * Test three-of-a-kind ranking.
	 */
	public void testThreeOfAKind() {
		assertTrue(threeOfAKind.isThreeOfAKind().isPresent());
	}
	
	/**
	 * Test straight ranking.
	 */
	public void testStraight() {
		assertTrue(straight.isStraight());
	}
	
	/**
	 * Test flush ranking.
	 */
	public void testFlush() {
		assertTrue(flush.isFlush());
	}
	
	/**
	 * Test full house ranking.
	 */
	public void testFullHouse() {
		assertTrue(fullHouse.isFullHouse().isPresent());
	}
	
	/**
	 * Test four-of-a-kind ranking.
	 */
	public void testFourOfAKind() {
		assertTrue(fourOfAKind.isFourOfAKind().isPresent());
	}
	
	/**
	 * Test straight flush ranking.
	 */
	public void testStraightFlush() {
		assertTrue(straightFlush.isStraightFlush());
	}
	
	/**
	 * Test straight flush ranking.
	 */
	public void testRoyakFlush() {
		assertTrue(royalFlushHand.isRoyalFlush());
	}

	
	/**
	 * compareTo testing.
	 */
	public void testCompareTo() {
		assertEquals(0, pair.compareTo(pair));
		assertEquals(-1, pair.compareTo(fullHouse));
		assertEquals(1, fullHouse.compareTo(pair));
		assertEquals(1, pair.compareTo(pair2));
		assertEquals(1, scoreOneHighestNumber.compareTo(scoreOneLeastNumber));
		assertEquals(0, royalFlushHand.compareTo(royalFlushHand2));
	}
}