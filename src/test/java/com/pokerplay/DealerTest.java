package com.pokerplay;

import java.util.*;


public class DealerTest extends junit.framework.TestCase {
	
    private Dealer dealer;
    private Dealer dealer1;
    private Dealer dealer2;
    private Dealer dealer3;
    private Dealer dealer4;
    private Dealer dealer5;
    private Dealer dealer6;
    private Dealer dealer7;
    
	public DealerTest() {
		dealer = new Dealer("9C 9D 8D 7C 3C 2S KD TH 9H 8H");
		dealer1 = new Dealer("JH QC AC KS 4S 5D 3H TC JS JC");
		dealer2 = new Dealer("8D 7D TC JS 7S JD 7H KD 2C 8C");
		dealer3 = new Dealer("AC 7S KS QD JD 3D 8D 9H TS TC");
		dealer4 = new Dealer("3C TD 7H 2H 3D QC 7S 6C AD 7D");
		dealer5 = new Dealer("5C 7H 7C 6H 9S 4D 9C 5D AC TD");
		dealer6 = new Dealer("8C 7D AH KC 9C 3H JS 4D 2D QC");
		dealer7 = new Dealer("AC 2D 7D 3C TC 3S JS 4H 4D JD");
	}
	
	/**
	 * Test the play result.
	 */
	public void testPlayResult() {
		assertEquals(1, dealer.getPlayResult());
		assertEquals(-1, dealer1.getPlayResult());
		assertEquals(1, dealer2.getPlayResult());
		assertEquals(-1, dealer3.getPlayResult());
		assertEquals(-1, dealer4.getPlayResult());
		assertEquals(1, dealer5.getPlayResult());
		assertEquals(1, dealer6.getPlayResult());
		assertEquals(-1, dealer7.getPlayResult());
	}
	
}